package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class Demo57Application {

	public static void main(String[] args) {

ApplicationContext ctx=new AnnotationConfigApplicationContext(
        Demo57Application.class);
    //By its type
//UserService user=ctx.getBean(UserService.class);2`SS
//System.out.println("printing user"+user.getList());
        //By its alias name
 UserService user=(UserService)ctx.getBean("myBean");
 System.out.println("printing user"+user.getList());
	}

}
